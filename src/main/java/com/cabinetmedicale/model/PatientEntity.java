package com.cabinetmedicale.model;

public class PatientEntity {
	

	private long id;
	
	private String numPatient;

	private String numCNSS;
	
	private String nom;
	
	private String prenom;
	
	private String email;
	
	private String numTel;
	
	private String addresse;
	
	private String login;
	
	private String password;
	
	
	//private DossierEntity dossierEntity;
	
	
	public PatientEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PatientEntity(long id, String numPatient, String numCNSS, String nom, String prenom, String email,
			String numTel, String addresse, String login, String password) {
		super();
		this.id = id;
		this.numPatient = numPatient;
		this.numCNSS = numCNSS;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.numTel = numTel;
		this.addresse = addresse;
		this.login = login;
		this.password = password;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNumPatient() {
		return numPatient;
	}
	public void setNumPatient(String numPatient) {
		this.numPatient = numPatient;
	}
	public String getNumCNSS() {
		return numCNSS;
	}
	public void setNumCNSS(String numCNSS) {
		this.numCNSS = numCNSS;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumTel() {
		return numTel;
	}
	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}
	public String getAddresse() {
		return addresse;
	}
	public void setAddresse(String addresse) {
		this.addresse = addresse;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	

}
