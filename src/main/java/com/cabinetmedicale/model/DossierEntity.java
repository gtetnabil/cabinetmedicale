package com.cabinetmedicale.model;

import java.util.List;

public class DossierEntity {
	
	
	private long id;
	private String numDossier;
	//private List<RapportEntity> rapports;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNumDossier() {
		return numDossier;
	}
	public void setNumDossier(String numDossier) {
		this.numDossier = numDossier;
	}


}
