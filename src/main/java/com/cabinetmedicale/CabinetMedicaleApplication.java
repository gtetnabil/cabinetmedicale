package com.cabinetmedicale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })

public class CabinetMedicaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CabinetMedicaleApplication.class, args);
	}

}
